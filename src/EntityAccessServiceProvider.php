<?php

namespace Drupal\entity_access;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\Core\Entity\EntityAccessCheck as EntityAccessCheckBase;
use Drupal\entity_access\Entity\EntityAccessCheck;

/**
 * The service provider.
 */
class EntityAccessServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    $entity_access_check_definition = $container->getDefinition('access_check.entity');
    \assert($entity_access_check_definition->getClass() === EntityAccessCheckBase::class);
    $entity_access_check_definition->setClass(EntityAccessCheck::class);
  }

}

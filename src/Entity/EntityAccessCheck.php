<?php

namespace Drupal\entity_access\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityAccessCheck as EntityAccessCheckBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\Routing\Route;

/**
 * The entity access check.
 */
class EntityAccessCheck extends EntityAccessCheckBase {

  /**
   * {@inheritdoc}
   *
   * Available improvement:
   * @code
   * example.route:
   *   path: /admin/structure/taxonomy/manage/{taxonomy_vocabulary}/update
   *   requirements:
   *     _entity_access: taxonomy_vocabulary:BUNDLE_NAME.update
   * @endcode
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {
    // Split the entity type and the operation.
    [$entity_type_id, $operation] = \explode('.', $route->getRequirement('_entity_access'), 2);
    $entity_type_id = \explode(':', $entity_type_id, 2);
    $entity = $route_match->getParameters()->get($entity_type_id[0]);

    if ($entity instanceof EntityInterface) {
      if (
        // No bundle specified - check the access as the core does.
        !isset($entity_type_id[1]) ||
        // The bundle specified - check if it matches the entity's one.
        /* @link https://www.drupal.org/node/2835597#comment-11825373 */
        ($entity instanceof ContentEntityInterface ? $entity->bundle() : $entity->id()) === $entity_type_id[1]
      ) {
        return $entity->access($operation, $account, TRUE);
      }
    }

    // No opinion, so other access checks should decide if access should be
    // allowed or not.
    return AccessResult::neutral();
  }

}
